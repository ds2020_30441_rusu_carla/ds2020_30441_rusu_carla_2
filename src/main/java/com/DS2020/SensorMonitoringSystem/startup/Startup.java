package com.DS2020.SensorMonitoringSystem.startup;

import com.DS2020.SensorMonitoringSystem.dtos.Activity;
import com.DS2020.SensorMonitoringSystem.producer.Producer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

@Component
public class Startup implements CommandLineRunner {

    @Autowired
    private Producer producer;

    @Override
    public void run(String... args) throws Exception {
        readFile();
    }

    // read from input file
    public void readFile() throws IOException {
        String fileName = "activity.txt";
        ClassLoader classLoader = getClass().getClassLoader();
        InputStream inputStream = classLoader.getResourceAsStream(fileName);

        // the stream holding the file content
        if (inputStream == null) {
            throw new IllegalArgumentException("file not found! " + fileName);
        } else {
            try (InputStreamReader streamReader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
                 BufferedReader reader = new BufferedReader(streamReader)) {

                String line;

                List<Activity> activities = new ArrayList<>();
                while ((line = reader.readLine()) != null) {
                    String delims = "[\t\t]+";
                    String[] tokens = line.split(delims);

                    Activity activity = new Activity(1L, tokens[2], Timestamp.valueOf(tokens[0]).getTime() / 1000L, Timestamp.valueOf(tokens[1]).getTime() / 1000L);
                    activities.add(activity);
                }
                producer.produceMsg(activities);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
