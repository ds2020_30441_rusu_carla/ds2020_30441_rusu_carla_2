package com.DS2020.SensorMonitoringSystem.producer;


import com.DS2020.SensorMonitoringSystem.dtos.Activity;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Component
public class Producer {

    @Autowired
    private AmqpTemplate amqpTemplate;

    // defined in app properties
    @Value("${rabbitmq.queue}")
    private  String queueName;

    @Value("${rabbitmq.exchange}")
    private String exchange;

    @Value("${rabbitmq.routingkey}")
    private String routingKey;

    // publish message on the queue
    public void produceMsg(List<Activity> activities) {
        ObjectMapper objectMapper = new ObjectMapper();

        activities.forEach(activity -> {
            try {
                amqpTemplate.convertAndSend(exchange, routingKey, activity);
                System.out.println(activity);
                TimeUnit.SECONDS.sleep(1);
                //TimeUnit.MILLISECONDS.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

}
