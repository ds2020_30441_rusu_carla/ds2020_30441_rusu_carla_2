package com.DS2020.SensorMonitoringSystem.dtos;

public class Activity {
    private Long patient_id;
    private String activity;
    private Long start;
    private Long end;

    public Activity(Long patient_id, String activity, Long start, Long end) {
        this.patient_id = patient_id;
        this.activity = activity;
        this.start = start;
        this.end = end;
    }

    @Override
    public String toString() {
        return "Activity{" +
                "patient_id=" + patient_id +
                ", activity='" + activity + '\'' +
                ", start=" + start +
                ", end=" + end +
                '}';
    }

    public Long getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(Long patient_id) {
        this.patient_id = patient_id;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public Long getStart() {
        return start;
    }

    public void setStart(Long start) {
        this.start = start;
    }

    public Long getEnd() {
        return end;
    }

    public void setEnd(Long end) {
        this.end = end;
    }
}
