package com.DS2020.SensorMonitoringSystem;

import com.DS2020.SensorMonitoringSystem.dtos.Activity;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;

@SpringBootApplication
public class SensorMonitoringSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(SensorMonitoringSystemApplication.class, args);
	}

}
