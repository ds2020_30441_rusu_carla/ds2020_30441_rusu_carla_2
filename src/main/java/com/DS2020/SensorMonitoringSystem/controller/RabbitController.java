//package com.DS2020.SensorMonitoringSystem.controller;
//
//import com.DS2020.SensorMonitoringSystem.producer.Producer;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.io.IOException;
//
//@RestController
//public class RabbitController {
//
//    @Autowired
//    Producer producer;
//
//    @GetMapping("/send")
//    public String sendMessage(@RequestParam("msg") String msg) throws IOException {
//        System.out.println("*****" + msg);
//        for (int i = 0; i<2;i++) {
//            producer.produceMsg(msg);
//        }
//        return "Successfully Msg Sent";
//    }
//}
